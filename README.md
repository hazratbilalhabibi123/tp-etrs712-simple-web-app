# TP-ETRS712-Simple-web-app

c'est un simple to-do application web, crée en php.


## Etaps pour crée un projet comme cela:

Premier on a crée un un repo sur Gitlab et puis on a cloner su notre machine local ( WORKING DIR )

# Clonner en utilisent protocole HTTP
```
git clone https://gitlab.com/hazratbilalhabibi123/tp-etrs712-simple-web-app.git
```
# Clonner en utilisent protocole SSH
```
git clone git@gitlab.com:hazratbilalhabibi123/tp-etrs712-simple-web-app.git
```

# Creer image à partir de Dockerfile
pour crée un image à partiie d'un ficheir Dockerfile, aller sur le repertoir où se trouve le ficheir Docker file et execute la commande suivant, si vous voulez executer le fichier Docker file à partir d'un autre repertoir il faut que vous saisi le chemin complet vers le Dockerfile sur place de .

```
docker build -t < CHOISIR UN NOM POUR IMAGE > .
docker build -t < CHOISIR UN NOM POUR IMAGE > < PATH TO DOCKERFILE >
```

## Executer l'image crée 
```
docker run -d -p 31110:80 try-tod
docker run -d  < PORT POUR ACCEDER APP > : < PORT D'APP > < NOM D'IMAGE >
```

## Commande utiles de docker

```
docker images
docker image ls
docker container ls
docker ps
docker ps - a -q

docker stop < CONTAINER ID>
docker rm  < CONTAINER ID >
docker rm -f  < CONTAINER ID >
docker rmi image < IMAGE >

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker exec -it <CONTAINER ID > bash

```


# Registre de conteneurs sur GitLab
pour afficher le registre de conteneurs pour un projet, accéder à votre projet et puis accéder  à Packages et registres > Registre de conteneurs que se trouve à gauch sur le sidebar.

## Créer et envoyer des images à l'aide des commandes Docker
pour crée et envoyer un image, on utilise les commande suivant vous pouvez trouvez ces commandes dans registres > Registre de conteneurs.
vous pouvez recrée votre image avec un nouveau tag simplement ajouter :NOM_DE_TAG à la fin de commande docker build ...
et pour metrre sur gitlab avec ce tag il faut ajouter aussi à la fin de commande docker push ..... :NOM_DE_TAG
```
docker build -t registry.example.com/group/project/image .
docker login registry.example.com -u <username> -p <token>
docker push registry.example.com/group/project/image
```

Les commande que j'ai utilisé
```
docker build -t registry.gitlab.com/hazratbilalhabibi123/tp-etrs712-simple-web-app todo-app
docker login registry.gitlab.com/hazratbilalhabibi123/tp-etrs712-simple-web-app
docker push registry.gitlab.com/hazratbilalhabibi123/tp-etrs712-simple-web-app:latest
```



# Docker-compose file
dans cette partir on a utilisé le fichier docker-compose pour notre l'application et mysql.

```
docker build -t registry.gitlab.com/hazratbilalhabibi123/tp-etrs712-simple-web-app todo-app
docker login registry.gitlab.com/hazratbilalhabibi123/tp-etrs712-simple-web-app
docker push registry.gitlab.com/hazratbilalhabibi123/tp-etrs712-simple-web-app:latest
```


## Commande utiles de docker-compose

```
docker-compose build
docker-compose up -d
docker-compose down
docker-compose exec -it < CONTAINER ID > bash
```
