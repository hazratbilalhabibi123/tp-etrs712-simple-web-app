<?php
include "config/db.php";

if(isset($_POST['submit']) && ($_POST['submit'] == "add") && !empty($_POST['name'])){
    $name = $_POST['name'];
    $sql = "INSERT INTO `tasks`(`name`) VALUES('$name')";

    $conn->exec($sql);
    header("Location: index.php");
}

//var_dump($_POST['submit']);

if(isset($_POST['submit']) && $_POST['submit'] == "delete"){
    $id = $_POST['id'];
    $sql = "DELETE FROM tasks  WHERE id=$id";
    
    $conn->exec($sql);

    header("Location: index.php");
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>


<div class="container">
    <div class="panel-body m-4">
        <!-- New Task Form -->
        <form action="/" method="POST" class="form-horizontal">
            <!-- Task Name -->
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Task</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control">
                </div>
            </div>
            <!-- Add Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button name="submit" value="add" class="btn btn-success">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>
        </form>
    </div>



    
<?php


if ($conn){
    ?>
    <table class="table table-striped task-table">
        <!-- Table Headings -->
        <thead>
            <tr>
                <th>Task</th>
                <th> Action </th>
            </tr>
        </thead>
        <!-- Table Body -->
        <tbody>
    <?php

    $sql = "select * from tasks";
    $result = $conn->query($sql);

    while ($data = $result->fetch()) {
    ?>

        <tr>
            <!-- Task Name -->
            <td class="table-text">
                <div> <?php echo $data['name']; ?> </div>
            </td>
             <!-- Delete Button -->
             <td>
                <form action="/" method="POST">
                    <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
                    <button name="submit"  value="delete"class="btn btn-danger">Delete Task</button>
                </form>
            </td>
        </tr>
    <?php
    }
    ?>
        </tbody>
    </table>

<?php

}




?>


</div>
</body>
</html>